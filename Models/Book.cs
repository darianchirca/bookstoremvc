﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookStoreMVC5.Models
{
    public class Book
    {
        public int BookId { get; set; }

        [Display(Name = "Topic")]
        public int? TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string Name { get; set; }
        [Required]
        public string ISBN { get; set; }
        [Required]
        public string Edition { get; set; }
        [Required]
        [StringLength(2000)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Display(Name = "Author")]
        public int? AuthorId { get; set; }
        public virtual Author Author { get; set; }
        [Display(Name = "Published")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? Year { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal? Price { get; set; }

        private string BookImage;

        public string Image
        {
            get { return BookImage; }
            set { BookImage = $"/Images/{Name.Trim()}.jpg"; }
        }

    }
}