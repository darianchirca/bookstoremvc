﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookStoreMVC5.Models
{
    public class Author
    {
        public Author()
        {
            Books = new List<Book>();
        }
        public int AuthorId { get; set; }
        [Required]
        [StringLength(100)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(2000)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public virtual ICollection<Book> Books { get; set; }

        private string AuthorImage;

        public string Image
        {
            get { return AuthorImage; }
            set { AuthorImage = $"/Images/{LastName}.jpg"; }
        }

    }
}