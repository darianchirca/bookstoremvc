﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookStoreMVC5.Models
{
    public class Topic
    {
        public int TopicId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Display(Name = "Books")]
        public int? BookId { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}