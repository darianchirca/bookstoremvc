﻿using System;
using System.Collections.Generic;
using System.Linq;
using PagedList;
using System.Web;

namespace BookStoreMVC5.Models
{
    public class BooksViewModel
    {
        public IPagedList<Book> Books { get; set; }
        public string Search { get; set; }
        public string SortBy { get; set; }
        public Dictionary<string, string> Sorts { get; set; }

    }
}