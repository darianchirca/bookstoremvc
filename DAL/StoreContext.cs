﻿using BookStoreMVC5.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookStoreMVC5.DAL
{
    public class StoreContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<BasketLine> BasketLines { get; set; }
    }
}