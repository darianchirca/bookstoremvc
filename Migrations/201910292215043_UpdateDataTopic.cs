namespace BookStoreMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDataTopic : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topics", "BookId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Topics", "BookId");
        }
    }
}
