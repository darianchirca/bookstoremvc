namespace BookStoreMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDataPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Year", c => c.DateTime());
            AddColumn("dbo.Books", "Price", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "Price");
            DropColumn("dbo.Books", "Year");
        }
    }
}
