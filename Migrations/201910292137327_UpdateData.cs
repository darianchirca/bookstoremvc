namespace BookStoreMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "TopicId", c => c.Int());
            CreateIndex("dbo.Books", "TopicId");
            AddForeignKey("dbo.Books", "TopicId", "dbo.Topics", "TopicId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "TopicId", "dbo.Topics");
            DropIndex("dbo.Books", new[] { "TopicId" });
            DropColumn("dbo.Books", "TopicId");
        }
    }
}
