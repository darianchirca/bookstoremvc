namespace BookStoreMVC5.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BookStoreMVC5.DAL.StoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BookStoreMVC5.DAL.StoreContext context)
        {
            //context.Topics.AddOrUpdate(
                
            //    new Models.Topic() { Name = "Adventure" },
            //    new Models.Topic() { Name = "Crime" },
            //    new Models.Topic() { Name = "History" },
            //    new Models.Topic() { Name = "Law" },
            //    new Models.Topic() { Name = "Novel" },
            //    new Models.Topic() { Name = "Science" },
            //    new Models.Topic() { Name = "Religion" }
                
            //    );

            //context.Authors.AddOrUpdate(
            //    new Models.Author() { FirstName = "Robert", LastName = "Langdon", Description = "Professor Robert Langdon is a fictional character created by author Dan Brown for his Robert Langdon book series: Angels & Demons (2000), The Da Vinci Code (2003), The Lost Symbol (2009), Inferno (2013) and Origin (2017).[1] He is a Harvard University professor of history of art and symbology (a fictional field related to the study of historic symbols, which is not methodologically connected to the actual discipline of semiotics)." }    
            //    );
        }
    }
}
