namespace BookStoreMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDataImages : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Authors", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Authors", "Image");
        }
    }
}
