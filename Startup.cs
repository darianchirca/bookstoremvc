﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BookStoreMVC5.Startup))]
namespace BookStoreMVC5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
