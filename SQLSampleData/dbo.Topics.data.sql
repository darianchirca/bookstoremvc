SET IDENTITY_INSERT [dbo].[Topics] ON
INSERT INTO [dbo].[Topics] ([TopicId], [Name], [BookId]) VALUES (1, N'Adventure', 1)
INSERT INTO [dbo].[Topics] ([TopicId], [Name], [BookId]) VALUES (2, N'Crime', 3)
INSERT INTO [dbo].[Topics] ([TopicId], [Name], [BookId]) VALUES (3, N'History', 4)
INSERT INTO [dbo].[Topics] ([TopicId], [Name], [BookId]) VALUES (4, N'Law', NULL)
INSERT INTO [dbo].[Topics] ([TopicId], [Name], [BookId]) VALUES (5, N'Novel', NULL)
INSERT INTO [dbo].[Topics] ([TopicId], [Name], [BookId]) VALUES (6, N'Science', NULL)
INSERT INTO [dbo].[Topics] ([TopicId], [Name], [BookId]) VALUES (7, N'Religion', NULL)
SET IDENTITY_INSERT [dbo].[Topics] OFF
